# Small practice examples of Java 8 Streams and lambda functions

 * master branch is mean for the practice
 * solutions branch has completed examples
 * cons branch has only the Eratosthenes sieve implementation copied from http://blog.informatech.cr/2013/03/11/java-infinite-streams/ by Edwin Dalorzo
 
## Import project to your IDE

### Intellij Idea:

In project root execute command to generate Idea project files

    ./gradlew idea

Then start Idea and choose Open Project from menu and select project root directory.

### Eclipse

Eclipse needs [Gradle plugin](https://github.com/spring-projects/eclipse-integration-gradle).