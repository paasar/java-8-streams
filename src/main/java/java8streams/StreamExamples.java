package java8streams;

import java8streams.headandtail.Cons;
import java8streams.headandtail.HeadAndTailStream;
import java8streams.person.Gender;
import java8streams.person.Person;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class StreamExamples {

    public List<Integer> mapPlusOne() {
        return asList(1, 2, 3)
                // Turn collection into a Stream.
                .stream()
                // Apply an addition function to each element.
                .map(
                        new Function<Integer, Integer>() {
                            @Override
                            public Integer apply(Integer input) {
                                return input;
                            }
                        }
                )
                // Transform result Stream into an ArrayList.
                // Object::method is called method reference.
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<Integer> mapPlusOneWithLambda() {
        return asList(1, 2, 3)
                .stream()
                .map(
                    // Use lambda function/syntax to make it more concise.
                    // eg. x -> y
                    // where x is input and y is output.
                    input -> input
                )
                // Collectors class offers shortcuts like toList()
                .collect(Collectors.toList());
    }

    public int addOneThroughFiveTogether() {
        // There are own Stream classes for primitive numbers.
        return 0;
//        return IntStream
//                // Create a Stream from 1 to 5
//                .rangeClosed(1, 5)
//                // Count them all together to produce single int.
//                .??();
    }

    // Order of operations is important.
    public List<Integer> limitAndOdd() {
        return asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
                .stream()
                .filter(n -> n % 2 != 0)
                .limit(5)
                .collect(Collectors.toList());
    }

    // Order of operations is important #2.
    public List<Integer> oddAndLimit() {
        return asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
                .stream()
                .limit(5)
                .filter(n -> n % 2 != 0)
                .collect(Collectors.toList());
    }

    public List<String> moreThanOneAtATime() {
        return asList("foo", "bar", "baz", "quux")
                // Instead of stream() you can use parallelStream() to make the computation run parallel.
                .stream()
                .map(s -> {
                            try {
                                // This is here to slow down each "operation".
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            return s;
                        }
                )
                .collect(Collectors.toList());
    }

    public int sumOfAges() {
        // Objects can be manipulated pretty easily in a Stream.
        // See Person class.
        return createFamily()
                .stream()
                // MapToInt produces an IntStream from an Object Stream using given function.
                // Again to simplify the syntax, instead of person -> person.getAge()
                // one can use method reference Person::getAge.
                .mapToInt(person -> 0)
                .sum();
    }

    public List<Person> sortByAge() {
        return createFamily()
                .stream()
                //Add a sort by age here
                .collect(Collectors.toList());
    }

    public Map<Gender, List<Person>> groupByGender() {
        return Collections.emptyMap();
//        return createFamily()
//                .stream()
//                // Elements can be grouped by some rule.
//                // Add a grouping by collector.
//                .collect(??);
    }

    private List<Person> createFamily() {
        return asList(createPerson("Petja", 11, Gender.MALE),
                createPerson("Pekka", 29, Gender.MALE),
                createPerson("Pinja", 7, Gender.FEMALE),
                createPerson("Paula", 31, Gender.FEMALE));
    }

    private Person createPerson(String name,
                                int age,
                                Gender gender) {
        return new Person(name, age, gender);
    }

    public List<String> biFunction() {
        Map<String, Integer> input = new HashMap<>();
        input.put("a", 1);
        input.put("b", 2);
        input.put("c", 3);

        List<String> result = new ArrayList<>();
        // Create a BiConsumer that turns key, ":" and value into a concatenated String
        // and adds it into result.
//        input.forEach(??);

        return result;
    }

    public Stream<Long> longGenerator() {
        // Long.MAX is the end of line.
        // Here is an example of a Supplier that produces values for a Stream.
        return Stream.generate(new Supplier<Long>() {
            private long value = 1;

            @Override
            public Long get() {
                return value++;
            }
        });
    }


    // Here are some examples how lazy evaluation can be used.
    //
    // http://blog.informatech.cr/2013/03/11/java-infinite-streams/
    // ------------------------------------------------------------

    //int generator with Cons
    public static HeadAndTailStream<Integer> from(int n) {
        return new Cons<>(n, () -> from(n+1));
    }

    //sieve of Eratosthenes
    private static HeadAndTailStream<Integer> sieve(HeadAndTailStream<Integer> s) {
        return new Cons<>(s.head(),
                          ()-> sieve(s.tail().filter(n -> n % s.head() != 0)));
    }

    public void primes(long n) {
        sieve(from(2))
                .takeWhile(m -> m < n)
                .forEach(System.out::println);
    }

    // Primes with Stream
    // http://codereview.stackexchange.com/a/58905
    public List<Integer> primesWithStream(int numPrimes) {
        final List<Integer> primes = new ArrayList<>(numPrimes);
        IntStream.iterate(2, i -> i + 1).
                filter(i -> {
                    for (int prime : primes)
                        if (i % prime == 0)
                            return false;
                    return true;
                }).limit(numPrimes).
                forEach(primes::add);
        return primes;
    }

    // Fibonacci using Cons
    public static HeadAndTailStream<Integer> fibonacci() {
        //first two fibonacci and a closure to get the rest.
        return new Cons<>(0,() -> new Cons<>(1, () -> nextFibPair(0,1)));
    }

    private static HeadAndTailStream<Integer> nextFibPair(int a, int b) {
        int fib = a + b;
        return new Cons<>(fib, () -> nextFibPair(b, fib));
    }

    public void fibonaccis(int limit) {
        fibonacci()
                .takeWhile(m -> m < limit)
                .forEach(System.out::println);
    }
}
