package java8streams.headandtail;

import java.util.function.Supplier;

public class Cons<T> implements HeadAndTailStream<T> {

    private final T head;
    //stream thunk
    private final Supplier<HeadAndTailStream<T>> tail;

    public Cons(T head, Supplier<HeadAndTailStream<T>> tail) {
        this.head = head;
        this.tail = tail;
    }

    public T head() {
        return this.head;
    }

    public HeadAndTailStream<T> tail() {
        //triggers closure evaluation
        return this.tail.get();
    }

    public boolean isEmpty() {
        return false;
    }
}
