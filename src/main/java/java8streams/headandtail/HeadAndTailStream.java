package java8streams.headandtail;

import java.util.function.Consumer;
import java.util.function.Predicate;

public interface HeadAndTailStream<T> {
    public T head();
    public HeadAndTailStream<T> tail();
    public boolean isEmpty();

    public default HeadAndTailStream<T> filter(Predicate<? super T> predicate) {
        return filter(this, predicate);
    }

    public static <T> HeadAndTailStream<T> filter(HeadAndTailStream<? extends T> source,
                                         Predicate<? super T> predicate) {
        if(source.isEmpty()) {
            return new Empty<>();
        }
        if(predicate.test(source.head())) {
            return new Cons<>(source.head(),
                              () -> filter(source.tail(), predicate));
        }
        return filter(source.tail(), predicate);
    }

    public default HeadAndTailStream<T> takeWhile(Predicate<? super T> predicate) {
        return takeWhile(this, predicate);
    }

    public static <T> HeadAndTailStream<T> takeWhile(HeadAndTailStream<? extends T> source,
                                            Predicate<? super T> predicate) {
        if(source.isEmpty() || !predicate.test(source.head())) {
            return new Empty<>();
        }
        //creates new cons cell and a closure for the rest
        return new Cons<>(source.head(),
                          () -> takeWhile(source.tail(), predicate));
    }

    public default void forEach(Consumer<? super T> consumer) {
        forEach(this, consumer);
    }

    public static <T>  void forEach(HeadAndTailStream<? extends T> source,
                                    Consumer<? super T> consumer) {
        while(!source.isEmpty()) {
            consumer.accept(source.head());
            //triggers closure evaluation
            source = source.tail();
        }
    }
}

