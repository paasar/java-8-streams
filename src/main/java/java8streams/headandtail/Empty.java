package java8streams.headandtail;

public class Empty<T> implements HeadAndTailStream<T> {
    public T head() {
        throw new UnsupportedOperationException("Empty stream");
    }
    public HeadAndTailStream<T> tail() {
        throw new UnsupportedOperationException("Empty stream");
    }
    public boolean isEmpty() { return true; }
}
