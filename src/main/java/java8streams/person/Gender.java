package java8streams.person;

public enum Gender {
    FEMALE,
    MALE
}
