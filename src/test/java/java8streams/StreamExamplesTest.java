package java8streams;

import java8streams.person.Gender;
import java8streams.person.Person;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotNull;

public class StreamExamplesTest {

    private StreamExamples examples;

    @Before
    public void setUp() {
        examples = new StreamExamples();
    }

    @Test
    public void plusOneEach() {
        assertEquals(
                asList(2, 3, 4),
                examples.mapPlusOne());
    }

    @Test
    public void plusOneEachWithLambda() {
        assertEquals(
                asList(2, 3, 4),
                examples.mapPlusOneWithLambda());
    }

    @Test
    public void addAllTogether() {
        assertEquals(15, examples.addOneThroughFiveTogether());
    }

    @Test
    public void limitAndOdd() {
        assertEquals(asList(1, 3, 5),
                examples.limitAndOdd());
    }

    @Test
    public void oddAndLimit() {
        assertEquals(asList(1, 3, 5, 7, 9),
                examples.oddAndLimit());
    }

    @Test
    public void moreThanOneAtATime() {
        long start = System.currentTimeMillis();
        examples.moreThanOneAtATime();
        long end = System.currentTimeMillis();

        long timeSpent = end - start;
        long expectedTime = 200;
        assertTrue("Took too much time: " + timeSpent + " vs. " + expectedTime + ".", timeSpent < expectedTime);
    }

    @Test
    public void sortingByField() {
        List<Person> sortedFamily = examples.sortByAge();
        assertEquals(7, sortedFamily.get(0).getAge());
        assertEquals(11, sortedFamily.get(1).getAge());
        assertEquals(29, sortedFamily.get(2).getAge());
        assertEquals(31, sortedFamily.get(3).getAge());
    }

    @Test
    public void sumOfAges() {
        assertEquals(78, examples.sumOfAges());
    }

    @Test
    public void groupByGender() {
        Map<Gender, List<Person>> result = examples.groupByGender();

        List<Person> females = result.get(Gender.FEMALE);
        List<Person> males = result.get(Gender.MALE);
        assertNotNull(females);
        assertEquals(2, females.stream()
                               .filter(person -> person.getGender() == Gender.FEMALE)
                               .collect(Collectors.toList())
                               .size());
        assertNotNull(males);
        assertEquals(2, males.stream()
                               .filter(person -> person.getGender() == Gender.MALE)
                               .collect(Collectors.toList())
                               .size());
    }

    @Test
    public void biFunction() {
        List<String> result = examples.biFunction();
        assertTrue("Should contain a:1", result.contains("a:1"));
        assertTrue("Should contain b:2", result.contains("b:2"));
        assertTrue("Should contain c:3", result.contains("c:3"));
    }

    @Test
    public void longGenerator() {
        assertEquals(15, examples.longGenerator()
                                 .limit(5)
                                 .mapToLong(l -> l)
                                 .sum());
    }

    //This just prints the result to console.
    @Test
    public void primes() {
        examples.primes(100);
    }

    @Test
    public void primesWithStream() {
        List<Integer> primes = examples.primesWithStream(20);
        assertEquals(asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71),
                     primes);
    }

    //This just prints the result to console.
    @Test
    public void fibonaccis() {
        examples.fibonaccis(1000);
    }
}